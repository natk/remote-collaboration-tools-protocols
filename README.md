# README #

This is an open source list of remote work and collaboration tools that might be handy
during (and after) the COVID19 crisis

### Tools I have tried ###

1. [Slack](https://www.slack.com): we have set up a group workspace, containing multiple different, channels to capture the usual chats that happen in a (shared) office and share some files, etc.
2. [Trello](https://www.trello.com): we use it to have a set of cards and list of work in progress (integrates nicely with Bitbucket and Jira), mostly for programming projects (and integrated with Bitbucket)
3. For Video Conferencing and Video-based communications:
    1. [Whereby](https://www.whereby.com): enables you to set up video chat rooms (VCRs) via the browser that you can keep open while working. Complements the short/fast messages in slack with deeper (when needed) discussions.
    2. [Zoom](https://www.zoom.us/): a high quality video and sound video conferencing tools with possibility of sharing files, calendar integration, etc.
    3. [Jitsi](https://meet.jit.si/): a fully features, secured and free (non-commercial) video conferencing tool.
    4. [Flipgrid](https://info.flipgrid.com/): a community video platform to share ideas and commentaries on user-defined topics and themes.
    5. [Loom](https://www.loom.com/): a wonderfully designed, simple, video capture tool to send and share short videos to team members or customers. Has a nice desktop-base app too.
4. For Sharing Brainstorming Canvases:
    1. [Miro](https://www.miro.com): to create collaborative on-line whiteboards with your team (integrates with Jira, Asana, etc)
    2. [Google Jamboard](https://tinyurl.com/t3ua93a): similar to Miro by google. You can access the free features from your google account (no need to pay subscription for basic features)
    3. [Padlet](https://padlet.com/): allows you (and collaborators) to create "padlets" containing images, diagrams, files for exploring and presenting ideas and information.
5. Projects and Teams Management:
    1. [Asana](https://www.asana.com): super-powered "to do" lists and integrations. Also has "cards" like Trello.
    2. [Basecamp](https://www.basecamp.com): keeps a distributed team up to date with work and maintains all files in the same place (similarly to Workli)
    3. [Workli](https://www.workliapp.com): A tool with a focus on deep work.  The only collaboration and project management tool with *integrated one-to-one and personalised coaching* to help individuals and teams become more productive. The focus is on high-level, project portfolios management, and overall productivity. It allows you to create recurrent collaboration cycles between decision makers and team members. Notifies only on a need-to-know basis thus reducing overheads. Keeps all files, notes, etc associated with the various project cycles (hence no need to search emails, dropbox, etc).
8. [Bitbucket](https://www.Bitbucket.com), [Github](https://www.Github.com), [Gitlab](https://www.gitlab.com): for version control of code and other stuff (like this list)
    1. Check the [Cheat Sheet](cheatsheets/GitCheetSheet.jpg) to familiarise yourself with Git commands and functions
9. [Whatsapp web](https://web.whatsapp.com): enables you to create a session linking your whatsapp account from your phone with your browser so you can more readily write and work (its difficult to type long stuff in a phone!)
10. [Soapbox](https://soapboxhq.com/): a collaborative agenda and note taking for remote meetings.
11. For Streaming and Broadcasting:
    1. [Open Broadcaster Software](https://obsproject.com/): an open source broadcasting studio.
    1. [Streamyard](https://streamyard.com/): browser based live streaming video and audio.
12. For online-workshops and conferences:
    1. [Remo](https://remo.co/conference/): A virtual conference with breakout tables for networking. The breakout tables work very nicely and are useful for working on small groups or discussing things at lenghts in reduced groups. The downside is that the app is pricy
    2. [Wonder](https://www.wonder.me/): online video-based gatherings with a social and (simulated) physical interface. Very easy to use via browser (desktop-based) and allows groups of people to seemlingly split up in groups and re-join groups by moving your avatar around.

### List to remote working tips, blogs, etc ###
* [Remote Tools and Tips in These Remote Times](https://thoughtbot.com/blog/remote-tools-and-tips-in-these-remote-times) by German Velasco from [thoughtbot](https://thoughtbot.com/)
* [How to Manage a Remote Team](https://zapier.com/learn/remote-work/how-manage-remote-team/) by Wade Foster from [zapier](https://zapier.com/home)
* Two great books by the creators of Basecamp: [Remote: office not required](https://amzn.to/2WtTqGR) and [Rework: change the way you work forever](https://amzn.to/2QyuQkl)
* [How to maintain a human-centered focus in a fully remote world](https://medium.com/enterprise-design-thinking/maintain-human-centered-focus-fully-remote-world-654fba515f96): a blog from IBM Enterprise Design Thinking Team.
* [GitLab's tips for a productive all-remote workforce](https://www.youtube.com/watch?v=CsLswGz6J5s): the largest all-remote company's CEO tips on remote work.
* [10 Apps and Gadgets for Becoming a More Productive Remote Worker](https://toomanyadapters.com/more-productive-remote-worker/)
* [Remote working guide](https://www.blurtitout.org/2020/03/18/remote-working-definitive-guide/): A thorough guide with tips for remote working, from tips for collaboration and managing your own unique home challenges, such as looking after children and lots of tips to keep your mental health in shape when working remotely
* [YC Advice on remote work](https://docs.google.com/document/d/1yFZGDIUE4r3TbqL9QZTHZz7jKyLwyvLqbuJ-s2litKI/edit): The famed YC has put together a list of resources and advice on how to transition a team to remote work and how to manage it.
* [Workli - the deep work company (Twitter account)](https://twitter.com/workliapp): regular posts with a focus on deep work, productivity and remote/hybrid work.
* [Workli - the deep work company (Facebook account)](https://www.facebook.com/Workliapp/): regular posts with a focus on deep work, productivity and remote/hybrid work.

### How to contribute

Please follow this simple steps:

1.  Fork the repo
2.  Create your feature branch (git checkout -b my-new-edits)
3.  Commit your changes (git commit -am 'Added some tools/blog/tips')
4.  Push to the branch (git push origin my-new-edits)
5.  Create new Pull Request

(but if you are not familiar with git then either check the [cheatsheets](cheatsheets/GitCheetSheet.jpg) or simply send me an email to `natalio.krasnogor@gmail.com` with your contribution)
